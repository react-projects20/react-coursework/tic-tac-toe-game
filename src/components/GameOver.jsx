export default function GameOver({winner, restartBtn}){
    return (
        <div id="game-over">
            <h2>Game Over!</h2>
            {winner && <p>{winner} won!</p>}
            {!winner && <p>It's a draw!</p>}
            <p>
            <button onClick={restartBtn}>REMATCH!</button>
            </p>
        </div>
    )
}