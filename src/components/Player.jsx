import React from 'react';
import { useState } from 'react';



export default function Player({intialName, symbol, isActive, onChangeName}){
   

    const [isEditing, setIsEditing] = useState(false);
    const [playerName, setPlayerName] = useState(intialName);

    function handleChange(event){
        setPlayerName(event.target.value)
    }

    function onClickHandler() {
        setIsEditing((editing)=>!editing)
        if (isEditing) {
            onChangeName(symbol,playerName)    
        }
    }

    let editablePlayerName = <span className="player-name">{playerName}</span>
    

    if (isEditing) {
        editablePlayerName = <input type='text' required value={playerName} onChange={handleChange}></input>
    } 



    return (
        <li className={isActive ? 'active': undefined}>
            <span className="player">
               {editablePlayerName} 
              <span className="player-symbol">{symbol}</span>
            </span>
            <button onClick={onClickHandler}>{isEditing ? 'Save ' : 'Edit'}</button>
        </li>
    )
}